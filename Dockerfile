FROM php:7.4.13-apache-buster

RUN  apt-get update -y && \
 apt-get install -y zip unzip git

RUN mkdir /var/www/.composer && chown -R www-data:www-data /var/www/.composer &&\
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" &&\
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" &&\
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer &&\
    php -r "unlink('composer-setup.php');"

COPY ./php.ini /usr/local/etc/php/php.ini
# configure xdebug to allow remote debugging in PhpStorm
RUN pecl install xdebug-3.0.3
RUN docker-php-ext-enable xdebug

ENV APACHE_DOCUMENT_ROOT=/var/www/html/examples
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

ENV PHP_IDE_CONFIG serverName=localhost:8080
ENV XDEBUG_SESSION PHPSTORM
#ENV XDEBUG_CONFIG remote_enable=1 remote_mode=req remote_port=9001 remote_host=172.17.0.1 remote_connect_back=0
ENV XDEBUG_CONFIG mode=debug client_host=172.17.0.1 client_port=9003 start_with_request=yes


RUN usermod -u 1000 www-data;
