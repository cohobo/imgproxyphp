<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule\ValueObject;


use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;

class Flag implements RuleValueObjectInterface
{
    private bool $value;
    private string $type;

    public function __construct(string $type, bool $value)
    {
        $sanitizedType = strtolower($type);
        if (!preg_match('/^[a-z]*$/', $sanitizedType)) {
            throw new InvalidArgumentException('Type can only has letters. Provided type was: ' . $type);
        }

        $this->value = $value;
        $this->type = $sanitizedType;
    }

    public function __toString()
    {
        return $this->value ? $this->type . ':1' : '';
    }

    public function value(): string
    {
        return $this->value ? '1' : '0';
    }
}
