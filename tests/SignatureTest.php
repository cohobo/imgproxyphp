<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Tests;

use Cohobo\ImgProxyPhp\Settings;
use Cohobo\ImgProxyPhp\Signature;

class SignatureTest extends \PHPUnit\Framework\TestCase
{
    private Settings $insecureSettings;
    private Settings $secureSettings;

    public function setUp(): void
    {
        $this->insecureSettings =  new Settings('http://localhost:8081');
        $this->secureSettings =  new Settings(
            'http://localhost:8081',
            'secret',
            'hello'
        );
    }
    public function testReturnsInsecureIfNoSaltAndKeyProvided()
    {
        $signature = new Signature($this->insecureSettings);

        $this->assertEquals(
            Signature::INSECURE_SIGNATURE,
            $signature->get('url/to/secure.jpg')
        );
    }

    public function testCanCalculateValidSignatureIfSaltAndKeyProvided()
    {
        $signature = new Signature($this->secureSettings);

        $url = 'c:120:30:sm/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.png';
        $this->assertEquals(
            '1HfygNAPywcJR5f7K00meD5cuHCMyvmIUCLG0BNaOHo',
            $signature->get($url)
        );
    }
}
