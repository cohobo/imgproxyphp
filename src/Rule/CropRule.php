<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule;

use Cohobo\ImgProxyPhp\Rule\ValueObject\FloatDimension;
use Cohobo\ImgProxyPhp\Rule\ValueObject\IntegerDimension;
use Cohobo\ImgProxyPhp\Rule\ValueObject\Gravity;

class CropRule implements RuleInterface
{
    private const CODE = 'c';
    private FloatDimension $width;
    private FloatDimension $height;
    private Gravity $gravity;

    public function __construct(FloatDimension $width, FloatDimension $height, Gravity $gravity)
    {
        $this->width = $width;
        $this->height = $height;
        $this->gravity = $gravity;
    }

    public static function fromParams(
        float $width,
        float $height,
        string $gravityType,
        float $offsetX = 0.0,
        float $offsetY = 0.0
    ) {
        return new self(
            new FloatDimension(FloatDimension::WIDTH, $width),
            new FloatDimension(FloatDimension::HEIGHT, $height),
            new Gravity($gravityType, $offsetX, $offsetY)
        );
    }

    public function __toString()
    {
        return implode(":", [
            self::CODE,
            $this->width->value(),
            $this->height->value(),
            $this->gravity->value()
        ]);
    }
}
