<?php

namespace Cohobo\ImgProxyPhp\Tests\Rule;

use Cohobo\ImgProxyPhp\Rule\ResizeRule;
use PHPUnit\Framework\TestCase;

class ResizeRuleTest extends TestCase
{
    /**
     * @dataProvider ruleDataProvider
     */
    public function testCreatedRuleReturnsValidRuleString($expected, $type, $w, $h, $enlarge, $extend)
    {
        $this->assertEquals($expected, (string) ResizeRule::fromParams($type, $w, $h, $enlarge, $extend));

    }

    public function ruleDataProvider()
    {
        return[
            ['', 'fit', 0, 0, 0, 0],
            ['rs:auto', 'auto', 0, 0, 0, 0],
            ['rs:fit:10:0:1', 'fit', 10, 0, 1, 0],
            ['rs:fill:10:20:1:1', 'fill', 10, 20, 1, 1],
        ];
    }
}
