<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule;

interface RuleInterface
{
    public function __toString();
}
