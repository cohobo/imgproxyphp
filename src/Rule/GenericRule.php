<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule;

class GenericRule implements RuleInterface
{
    private string $rule;

    private function __construct(
        string $rule
    ) {
        $this->rule = $rule;
    }

    public static function fromParams(string $rule): self
    {
        return new self($rule);
    }

    public function __toString()
    {
        return $this->rule;
    }
}
