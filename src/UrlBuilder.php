<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp;

use Cohobo\ImgProxyPhp\Rule\RuleInterface;

class UrlBuilder
{
    private Settings $settings;
    private ?Format $format = null;
    private Signature $signature;
    /** @var RuleInterface[] */
    private array $rules = [];

    public function __construct(
        Settings $settings,
        Signature $signature
    ) {
        $this->settings = $settings;
        $this->signature = $signature;
    }

    public function addRule(RuleInterface $rule): self
    {
        array_push($this->rules, $rule);
        return $this;
    }

    public function setFormat(Format $format): self
    {
        $this->format = $format;
        return $this;
    }

    public function generateUrl(string $sourceImageUrl): string
    {
        $url = $this->rulesString()
            . base64_encode($sourceImageUrl)
            . DIRECTORY_SEPARATOR
            .'.' . ($this->format ? $this->format->get() : pathinfo($sourceImageUrl, PATHINFO_EXTENSION));

        return $this->settings->getServer() . $this->signature->get($url) . DIRECTORY_SEPARATOR . $url;
    }

    private function rulesString(): string
    {
        if (empty($this->rules)) {
            return '';
        }

        return implode(DIRECTORY_SEPARATOR, $this->rules) . DIRECTORY_SEPARATOR;
    }
}
