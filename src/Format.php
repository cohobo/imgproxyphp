<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp;

class Format
{
    private string $format;

    private function __construct(string $format)
    {
        $this->format = $format;
    }

    public function get(): string
    {
        return $this->format;
    }

    public static function jpeg(): self
    {
        return new self('jpeg');
    }

    public static function png(): self
    {
        return new self('png');
    }

    public static function webp(): self
    {
        return new self('webp');
    }

    public static function gif(): self
    {
        return new self('gif');
    }
}
