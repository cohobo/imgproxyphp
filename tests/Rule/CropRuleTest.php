<?php

namespace Cohobo\ImgProxyPhp\Tests\Rule;

use Cohobo\ImgProxyPhp\Rule\CropRule;
use PHPUnit\Framework\TestCase;

class CropRuleTest extends TestCase
{
    /**
     * @dataProvider gravityValidProvider
     */
    public function testCanReturnCorrectRule($expected, $width, $height, $gravityType, $offsetX, $offsetY)
    {
        $this->assertEquals($expected, (string) CropRule::fromParams($width, $height, $gravityType, $offsetX, $offsetY));
    }

    public function gravityValidProvider()
    {
        return[
            ['c:100:100:sm', 100, 100, 'sm', 0, 0],
            ['c:100:100:ce:0:0', 100, 100, 'ce', 0, 0],
            ['c:100:100:ce:25:125', 100, 100, 'ce', 25, 125],
        ];
    }
}
