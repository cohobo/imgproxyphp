<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Tests;

class GenericRuleTest extends \PHPUnit\Framework\TestCase
{
    public function testCanBeCreatedFromValidString(): void
    {
        $this->assertInstanceOf(
            \Cohobo\ImgProxyPhp\Rule\GenericRule::class,
            \Cohobo\ImgProxyPhp\Rule\GenericRule::fromParams('w:500')
        );
    }
}
