<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule\ValueObject;

use \Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;

class ResizeType implements RuleValueObjectInterface
{
    public const FIT = 'fit';
    public const FIL = 'fill';
    public const AUTO = 'auto';
    public const EMPTY = '';
    private const CODE = 'rt';
    private const ALLOWED_TYPES = [
        self::FIT,
        self::FIL,
        self::AUTO,
        self::EMPTY
    ];

    private string $type;

    public function __construct(string $type)
    {
        $sanitizedType = strtolower($type);

        if (!in_array($sanitizedType, self::ALLOWED_TYPES)) {
            throw InvalidArgumentException::fromNotAllowedValueList($type, self::ALLOWED_TYPES);
        }

        $this->type = $sanitizedType;
    }

    public function __toString()
    {
        return in_array($this->type, [self::EMPTY, self::FIT])? '' : self::CODE . ':' . $this->type;
    }

    public function value(): string
    {
        return $this->type;
    }
}
