<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule\ValueObject;

use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;
use Cohobo\ImgProxyPhp\Rule\ShortRuleTrait;

class Gravity implements RuleValueObjectInterface
{
    use ShortRuleTrait;

    public const NORTH = 'no';
    public const SOUTH = 'so';
    public const EAST = 'ea';
    public const WEST = 'we';
    public const NORTH_EAST = 'noea';
    public const NORTH_WEST = 'nowe';
    public const SOUTH_EAST = 'soea';
    public const SOUTH_WEST = 'sowe';
    public const CENTER = 'ce';
    public const SMART = 'sm';
    public const FOCUS_POINT = 'fp';

    private const CODE = 'g';
    private const ALLOWED_VALUES = [
        self::NORTH,
        self::SOUTH,
        self::EAST,
        self::WEST,
        self::NORTH_EAST,
        self::NORTH_WEST,
        self::SOUTH_EAST,
        self::SOUTH_WEST,
        self::CENTER,
        self::SMART,
        self::FOCUS_POINT,
    ];
    private const DEFAULT_VALUES = [
        self::CENTER,
        0.0,
        0.0,
    ];

    private string $type;
    private float $x;
    private float $y;

    /**
     * @throws InvalidArgumentException
     */
    public function __construct(
        string $type,
        float $x = 0,
        float $y = 0
    ) {
        $sanitizedType = strtolower($type);

        $this->throwExceptionOnInvalidArgument($sanitizedType, $x, $y);

        $this->type = $sanitizedType;
        $this->x = $x;
        $this->y = $y;
    }

    public function __toString()
    {
        return $this->shortenRule([
                $this->type,
                $this->x,
                $this->y,
            ], self::DEFAULT_VALUES);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function throwExceptionOnInvalidArgument(string $sanitizedType, $x, $y): void
    {
        if (!in_array($sanitizedType, self::ALLOWED_VALUES)) {
            throw InvalidArgumentException::fromNotAllowedValueList($sanitizedType, self::ALLOWED_VALUES);
        }

        if ($x < 0) {
            throw InvalidArgumentException::fromNegativeNumber($x);
        }

        if ($y < 0) {
            throw InvalidArgumentException::fromNegativeNumber($y);
        }

        if ($sanitizedType === self::SMART && ($x !== 0.0 || $y !== 0.0)) {
            throw new InvalidArgumentException('Smart Gravity accepts no additional parameter - use 0 for x and y');
        }

        if ($sanitizedType === self::FOCUS_POINT && ($x > 1 || $y > 1 || $x < 0 || $y < 0)) {
            throw new InvalidArgumentException(sprintf(
                'Value must by bettwen 0 and 1, provided offset was %s, %s ',
                $x,
                $y
            ));
        }
    }

    public function value(): string
    {
        if ($this->type === self::SMART) {
            return self::SMART;
        }

        return implode(
            ':',
            [$this->type, $this->x, $this->y]
        );
    }
}
