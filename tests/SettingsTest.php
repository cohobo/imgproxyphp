<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Tests;

use PHPUnit\Framework\TestCase;

class SettingsTest extends TestCase
{
    private \Cohobo\ImgProxyPhp\Settings $insecureSettings;
    private \Cohobo\ImgProxyPhp\Settings $secureSettings;

    protected function setUp(): void
    {
        $this->insecureSettings =  new \Cohobo\ImgProxyPhp\Settings('http://localhost:8081');
        $this->secureSettings =  new \Cohobo\ImgProxyPhp\Settings(
            'http://localhost:8081',
            'secret',
            'hello'
        );
    }

    public function testCanReturnValidSalt()
    {
        $this->assertEquals('hello', $this->secureSettings->getSalt());
        $this->assertEquals(bin2hex('hello'), $this->secureSettings->getSalt(true));
        $this->assertEquals('', $this->insecureSettings->getSalt());
    }

    public function testCanReturnValidSecretKey()
    {
        $this->assertEquals('secret', $this->secureSettings->getSecretKey());
        $this->assertEquals(bin2hex('secret'), $this->secureSettings->getSecretKey(true));
        $this->assertEquals('', $this->insecureSettings->getSecretKey());

    }
}
