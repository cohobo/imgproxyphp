<?php

namespace Cohobo\ImgProxyPhp\Tests\Rule\ValueObject;

use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;
use Cohobo\ImgProxyPhp\Rule\ValueObject\Flag;
use PHPUnit\Framework\TestCase;

class FlagTest extends TestCase
{
    /**
     * @dataProvider invalidTypesProvider
     */
    public function testInvalidTypeThrowsException($type)
    {
        $this->expectException(InvalidArgumentException::class);
        new Flag($type, true);
    }

    public function testFalseFlagReturnEmptyString()
    {
        $this->assertEquals('', (string) new Flag('e', 0));
    }

    /**
     * @dataProvider validFlagsProvider
     */
    public function testCanReturnCorrectFlag($expect, $type, $value)
    {
        $this->assertEquals($expect, (string) new Flag($type, $value));

    }

    /**
     * @dataProvider valueDataProvider
     */
    public function testCanReturnCorrectStringifiedValue(string $expected, string $type, $value)
    {
        $this->assertEquals($expected, (new Flag($type, (bool) $value))->value());
    }

    public function invalidTypesProvider(): array
    {
        return[
            [' ab'],
            ['der1'],
            ['iJd$']
        ];
    }

    public function validFlagsProvider(): array
    {
        return[
            ['e:1', 'e', 1],
            ['test:1', 'test', 1],
            ['', 'test', 0],
            ['g:1', 'g', true]
        ];
    }

    public function valueDataProvider(): array
    {
        return[
            ['0', 'test', 0],
            ['0', 'test', false],
            ['1', 'test', true],
            ['1', 'test', 1],
        ];
    }
}
