<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule;


trait ShortRuleTrait
{
    public function shortenRule(array $values, array $defaults): string
    {
        $lastNonDefaultIndex = null;

        for ($i = count($values) - 1; $i >= 0; $i--) {
            if ($defaults[$i] !== $values[$i]) {
                $lastNonDefaultIndex = $i;
                break;
            }
        }

        return $lastNonDefaultIndex === null ? '' : self::CODE . ':' . implode(':', array_slice($values, 0, $lastNonDefaultIndex + 1));
    }
}
