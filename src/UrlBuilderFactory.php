<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp;

class UrlBuilderFactory
{
    private Settings $settings;

    public function __construct(
        Settings $settings
    ) {
        $this->settings = $settings;
    }

    public function create(): UrlBuilder
    {
        return new UrlBuilder($this->settings, new Signature($this->settings));
    }
}
