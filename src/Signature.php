<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp;

class Signature
{
    public const INSECURE_SIGNATURE = 'insecure';
    private Settings $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function get($url): string
    {
        if (!$this->settings->getSalt() || !$this->settings->getSecretKey()) {
            return self::INSECURE_SIGNATURE;
        }

        $encodedSignature = base64_encode(
            hash_hmac(
                'sha256',
                $this->settings->getSalt() . DIRECTORY_SEPARATOR . $url,
                $this->settings->getSecretKey(),
                true
            )
        );

        return str_replace(["+", "/", "="], ["-", "_", ""], $encodedSignature);
    }
}
