<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Tests\Rule\ValueObject;

use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;
use Cohobo\ImgProxyPhp\Rule\ValueObject\FloatDimension;
use Cohobo\ImgProxyPhp\Rule\ValueObject\IntegerDimension;

class DimensionTest extends \PHPUnit\Framework\TestCase
{
    public function testCanReturnFloatDimension()
    {
        $this->assertEquals('w:0.2', (string) new FloatDimension(FloatDimension::WIDTH, 0.2));
    }

    public function testCantCreateNegativeDimension()
    {
        $this->expectException(\Cohobo\ImgProxyPhp\Exception\InvalidArgumentException::class);
        new IntegerDimension(IntegerDimension::WIDTH, -5);
    }

    public function testCanCreateValidDimensions()
    {
        $this->assertInstanceOf(IntegerDimension::class, new IntegerDimension(IntegerDimension::WIDTH, 0));
        $this->assertInstanceOf(IntegerDimension::class, new IntegerDimension(IntegerDimension::HEIGHT, 25));
        $this->assertInstanceOf(FloatDimension::class, new FloatDimension(FloatDimension::HEIGHT, 25.5));
    }

    public function testThrowsExceptionOnInvalidType()
    {
        $this->expectException(InvalidArgumentException::class);
        new IntegerDimension('s', 25);
    }

    /**
     * @dataProvider validDimensionsProvider
     */
    public function testReturnCorrectDimensionString($expect, $type, $value)
    {
        $this->assertEquals(
            $expect,
            (string) new IntegerDimension($type, $value)
        );
    }

    /**
     * @dataProvider valueDataProvider
     */
    public function testCanReturnCorrectStringifiedValue(string $expected, string $type, int $value)
    {
        $this->assertEquals($expected, (new IntegerDimension($type, $value))->value());
    }

    public function validDimensionsProvider()
    {
        return[
            ['', 'w', 0],
            ['', 'h', 0],
            ['w:12', 'w', 12],
            ['h:34', 'h', 34],
        ];
    }

    public function valueDataProvider(): array
    {
        return[
            ['0', 'w', 0],
            ['0', 'h', 0],
            ['12', 'w', 12],
            ['34', 'h', 34],
        ];
    }
}
