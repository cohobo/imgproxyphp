<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Exception;


class InvalidArgumentException extends ImgProxyException
{
    public static function fromNotAllowedValueList($value, array $allowedValues): self
    {
        return new self(sprintf(
            'Argument %s is not allowed. Allowed values: [%s]',
            $value,
            implode(', ', $allowedValues)
        ));
    }

    public static function fromNegativeNumber($value)
    {
        return new self(sprintf(
            'Value can not be nagative. (%s) is negative value',
            $value
        ));

    }
}
