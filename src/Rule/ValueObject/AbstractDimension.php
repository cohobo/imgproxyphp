<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule\ValueObject;


use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;

abstract class AbstractDimension implements RuleValueObjectInterface
{
    public const WIDTH = 'w';
    public const HEIGHT = 'h';

    private const ALLOWED_TYPES = [self::WIDTH, self::HEIGHT];

    private string $value;
    private string $type;

    public function __construct(string $type, float $value)
    {
        $sanitizedType = strtolower($type);

        if (!in_array($sanitizedType, self::ALLOWED_TYPES)) {
            throw InvalidArgumentException::fromNotAllowedValueList($sanitizedType, self::ALLOWED_TYPES);
        }

        if ($value < 0) {
            throw InvalidArgumentException::fromNegativeNumber($value);
        }

        $this->value = (string) $value;
        $this->type = $sanitizedType;
    }

    public function __toString()
    {
        return $this->value ? $this->type . ':' . $this->value() : '';
    }

    public function value(): string
    {
        return $this->value;
    }
}
