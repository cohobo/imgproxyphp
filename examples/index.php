<?php

use Cohobo\ImgProxyPhp\Format;
use Cohobo\ImgProxyPhp\Rule\GenericRule;
use Cohobo\ImgProxyPhp\Rule\ResizeRule;
use Cohobo\ImgProxyPhp\UrlBuilderFactory;

include_once __DIR__ . "/../vendor/autoload.php";

$settings = new \Cohobo\ImgProxyPhp\Settings(
    'http://localhost:8081',
    'secret',
    'hello'
);
$urlFactory = new UrlBuilderFactory($settings);

$s1 = 'https://via.placeholder.com/400x400';
$resultS1 = $urlFactory
    ->create()
    ->setFormat(Format::webp())
    ->addRule(GenericRule::fromParams('s:200'))
    ->generateUrl($s1);

$s2 = 'https://via.placeholder.com/400x400';
$resultS2 = $urlFactory
    ->create()
    ->setFormat(Format::webp())
    ->addRule(ResizeRule::fromParams('fill', 200, 400))
    ->generateUrl($s1);

?>



<html lang="en">
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Example transformations</title>
    </head>
    <body>
        <h1>Example image transformations</h1>
        <table class="table table-striped">
            <tr>
                <td>
                    Original image 400px jpg
                </td>
                <td>
                    Image scaled to 200px and changed to webp
                </td>
            </tr>
            <tr>
                <td>
                    <img src="<?= $s1 ?>" alt="original image"/>
                </td>
                <td>
                    <img src="<?= $resultS1 ?>" alt="original image"/>
                    <br />
                    <?= $resultS1 ?>
                </td>
            </tr>

            <tr>
                <td>
                    Original image 400px jpg
                </td>
                <td>
                    Image scaled to 200px and changed to webp
                </td>
            </tr>
            <tr>
                <td>
                    <img src="<?= $s2 ?>" alt="original image"/>
                </td>
                <td>
                    <img src="<?= $resultS2 ?>" alt="original image"/>
                    <br />
                    <?= $resultS2 ?>
                </td>
            </tr>
        </table>
    </body>
</html>
