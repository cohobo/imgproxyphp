<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule\ValueObject;


interface RuleValueObjectInterface
{
    public function __toString();

    public function value(): string;
}
