<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Exception;

class ImgProxyException extends \Exception
{
}
