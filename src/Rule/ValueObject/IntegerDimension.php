<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule\ValueObject;


use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;

class IntegerDimension extends AbstractDimension
{
    public function __construct(string $type, int $value)
    {
        parent::__construct($type, $value);
    }

}
