<?php

namespace Cohobo\ImgProxyPhp\Tests\Rule\ValueObject;

use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;
use Cohobo\ImgProxyPhp\Rule\ValueObject\Gravity;
use PHPUnit\Framework\TestCase;

class GravityTest extends TestCase
{
    public function testThrowsExceptionOnIncorrectGravityType()
    {
        $this->expectException(InvalidArgumentException::class);
        new Gravity('bc');
    }

    /**
     * @dataProvider smartGravityErrorProvider
     */
    public function testSmartGravityThrowExceptionIfOffsetProvided(float $x, float $y)
    {
        $this->expectException(InvalidArgumentException::class);
        new Gravity(Gravity::SMART, $x, $y);
    }

    /**
     * @dataProvider negativeOffsetProvider
     */
    public function testThrowsExceptionOnNegativeOffset($x, $y)
    {
        $this->expectException(InvalidArgumentException::class);
        new Gravity(Gravity::SOUTH_WEST, $x, $y);
    }

    /**
     * @dataProvider focusPointGravityErrorProvider
     */
    public function testThrowsExceptionIfFocusPointGravityOffsetIsNotBetweenZeroAndOne($x, $y)
    {
        $this->expectException(InvalidArgumentException::class);
        new Gravity(Gravity::FOCUS_POINT, $x, $y);
    }

    /**
     * @dataProvider validGravityProvider
     */
    public function testOptimizedGravityStringIsCreated($expected, $type, $x, $y)
    {
        $this->assertEquals(
            $expected,
            (string) new Gravity($type, $x, $y)
        );
    }

    /**
     * @dataProvider valueDataProvider
     */
    public function testCanReturnCorrectStringifiedValue(string $expected, string $type, float $x, float $y)
    {
        $this->assertEquals($expected, (new Gravity($type, $x, $y))->value());
    }

    public function testSmartGravityValueDoesNotReturnDimensions()
    {
        $this->assertEquals(Gravity::SMART, (new Gravity(Gravity::SMART, 0, 0))->value());
    }


    public function smartGravityErrorProvider(): array
    {
        return [
            [0,1],
            [1.1, 5],
            [11,5],
        ];
    }

    public function negativeOffsetProvider(): array
    {
        return[
            [0, -1],
            [-1, 0],
            [-5, -1],
        ];
    }
    public function focusPointGravityErrorProvider(): array
    {
        return[
            [0, 1.1],
            [-1, 0],
            [5, 1],
            [5, 2]
        ];
    }

    public function validGravityProvider(): array
    {
        return[
            ['g:sm', 'sm', 0, 0],
            ['g:fp:0.2', 'fp', 0.2, 0],
            ['g:fp:0.2:1', 'fp', 0.2, 1],
            ['g:ce:30:10', 'ce', 30, 10],
        ];
    }

    public function valueDataProvider(): array
    {
        return[
            ['fp:0.2:0', 'fp', 0.2, 0],
            ['fp:0.2:1', 'fp', 0.2, 1],
            ['ce:30:10', 'ce', 30, 10],
            //smart value shouldn't return dimensions
            ['sm', 'sm', 0, 0]
        ];
    }
}
