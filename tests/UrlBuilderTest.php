<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Tests;

use Cohobo\ImgProxyPhp\Rule\GenericRule;
use Cohobo\ImgProxyPhp\Settings;
use Cohobo\ImgProxyPhp\UrlBuilder;
use Cohobo\ImgProxyPhp\UrlBuilderFactory;

class UrlBuilderTest extends \PHPUnit\Framework\TestCase
{
    private UrlBuilder $insecureBuilder;
    private UrlBuilder $secureBuilder;

    protected function setUp(): void
    {
        $insecureUrlBuilderFactory = new UrlBuilderFactory(new Settings('http://localhost:8081'));
        $this->insecureBuilder = $insecureUrlBuilderFactory->create();

        $secureUrlBuilderFactory = new UrlBuilderFactory(new Settings(
            'http://localhost:8081',
            'secret',
            'hello'
        ));
        $this->secureBuilder = $secureUrlBuilderFactory->create();

        parent::setUp();
    }

    public function testCanGenerateInsecureUrl()
    {
        $this->assertEquals(
            'http://localhost:8081/insecure/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.png',
            $this->insecureBuilder->generateUrl(
                'https://via.placeholder.com/350x150.png'
            )
        );
    }

    public function testCanGenerateImageWithOneRule()
    {
        $this->assertEquals(
            'http://localhost:8081/insecure/w:500/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.png',
            $this->insecureBuilder
                ->addRule(GenericRule::fromParams('w:500'))
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );
    }

    public function testCanGenerateImageWithManyRules()
    {
        $this->assertEquals(
            'http://localhost:8081/insecure/c:120:30:sm/w:30/h:30/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.png',
            $this->insecureBuilder
                ->addRule(GenericRule::fromParams('c:120:30:sm'))
                ->addRule(GenericRule::fromParams('w:30'))
                ->addRule(GenericRule::fromParams('h:30'))
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );
    }

    public function testCanGenerateImageWithChangedFormat()
    {
        $urlBuilder = $this->insecureBuilder
            ->addRule(GenericRule::fromParams('c:120:30:sm'));

        $this->assertEquals(
            'http://localhost:8081/insecure/c:120:30:sm/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.jpeg',
            $urlBuilder
                ->setFormat(\Cohobo\ImgProxyPhp\Format::jpeg())
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );

        $this->assertEquals(
            'http://localhost:8081/insecure/c:120:30:sm/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.gif',
            $urlBuilder
                ->setFormat(\Cohobo\ImgProxyPhp\Format::gif())
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );

        $this->assertEquals(
            'http://localhost:8081/insecure/c:120:30:sm/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.webp',
            $urlBuilder
                ->setFormat(\Cohobo\ImgProxyPhp\Format::webp())
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );
    }

    public function testCanPreserveExtension()
    {
        $this->assertEquals(
            'http://localhost:8081/insecure/c:120:30:sm/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.png',
            $this->insecureBuilder
                ->addRule(GenericRule::fromParams('c:120:30:sm'))
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );
    }

    public function testCanGenerateSecuredUrl()
    {
        $this->assertEquals(
            'http://localhost:8081/1HfygNAPywcJR5f7K00meD5cuHCMyvmIUCLG0BNaOHo/c:120:30:sm/aHR0cHM6Ly92aWEucGxhY2Vob2xkZXIuY29tLzM1MHgxNTAucG5n/.png',
            $this->secureBuilder
                ->addRule(GenericRule::fromParams('c:120:30:sm'))
                ->generateUrl(
                    'https://via.placeholder.com/350x150.png'
                )
        );
    }
}
