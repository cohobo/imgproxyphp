<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Rule;

use Cohobo\ImgProxyPhp\Exception\InvalidArgumentException;
use Cohobo\ImgProxyPhp\Rule\ValueObject\Flag;
use Cohobo\ImgProxyPhp\Rule\ValueObject\IntegerDimension;
use Cohobo\ImgProxyPhp\Rule\ValueObject\ResizeType;

class ResizeRule implements RuleInterface
{
    use ShortRuleTrait;
    private const CODE = 'rs';

    private const DEFAULT_VALUES = [
        ResizeType::FIT, '0', '0', '0', '0'
    ];

    private ResizeType $resizeType;
    private IntegerDimension $width;
    private IntegerDimension $height;
    private Flag $enlarge;
    private Flag $extend;

    public function __construct(
        ResizeType $resizeType,
        IntegerDimension $width,
        IntegerDimension $height,
        Flag $enlarge,
        Flag $extend
    ) {
        $this->resizeType = $resizeType;
        $this->width = $width;
        $this->height = $height;
        $this->enlarge = $enlarge;
        $this->extend = $extend;
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function fromParams(
        string $resizeType = '',
        int $width = 0,
        int $height = 0,
        bool $enlarge = false,
        bool $extend = false
    ): self {
        return new self(
            new ResizeType($resizeType),
            new IntegerDimension(IntegerDimension::WIDTH, $width),
            new IntegerDimension(IntegerDimension::HEIGHT, $height),
            new Flag('el', $enlarge),
            new Flag('ex', $extend)
        );
    }

    public function __toString()
    {
        $values = [
            (string) $this->resizeType->value(),
            (string) $this->width->value(),
            (string) $this->height->value(),
            (string) $this->enlarge->value(),
            (string) $this->extend->value()
        ];

        return $this->shortenRule($values, self::DEFAULT_VALUES);
    }
}
