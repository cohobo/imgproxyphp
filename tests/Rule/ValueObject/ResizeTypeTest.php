<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp\Tests\Rule\ValueObject;

use Cohobo\ImgProxyPhp\Rule\ValueObject\ResizeType;

class ResizeTypeTest extends \PHPUnit\Framework\TestCase
{
    public function testCanNotCrateInvalidType()
    {
        $this->expectException(\Cohobo\ImgProxyPhp\Exception\InvalidArgumentException::class);
        new ResizeType('invalid');
    }

    /**
     * @dataProvider typeProvider
     */
    public function testCanCrateValidTypes($expected, $type)
    {
        $this->assertEquals($expected, (string) new ResizeType($type));
    }

    public function typeProvider(): array
    {
        return [
            ['', ''],
            ['', 'fit'],
            ['', 'FiT'],
            ['rt:auto', 'auto'],
            ['rt:fill', 'fill'],
        ];
    }

}
