<?php

declare(strict_types=1);

namespace Cohobo\ImgProxyPhp;

class Settings
{
    private string $server;
    private string $secretKey;
    private string $salt;

    public function __construct(
        string $server,
        string $secretKey = '',
        string $salt = ''
    ) {
        $this->server = substr($server, -1, 1) === DIRECTORY_SEPARATOR ? $server : $server . DIRECTORY_SEPARATOR;
        $this->secretKey = $secretKey;
        $this->salt = $salt;
    }

    public function getServer(): string
    {
        return $this->server;
    }

    public function getSecretKey($hex = false): string
    {
        return $hex ? bin2hex($this->secretKey) : $this->secretKey;
    }

    public function getSalt($hex = false): string
    {
        return $hex ? bin2hex($this->salt) : $this->salt;
    }
}
